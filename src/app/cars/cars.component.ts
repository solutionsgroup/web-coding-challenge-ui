import { Component, OnInit } from '@angular/core';
import {CarService} from '../car.service';
import {Car} from '../car';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  dataSource: Car[];

  displayedColumns: string[] = ['year', 'make', 'model'];

  constructor(private carService: CarService) { }

  ngOnInit() {
    this.findAllCars();
  }

  findAllCars(): void {
    // TODO: Make call to appropriate service to obtain data to display in table.
  }
}
