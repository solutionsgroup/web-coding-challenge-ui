export class Car {
  id: number;
  year: number;
  make: string;
  model: string;
  // TODO: Add color property.
}
