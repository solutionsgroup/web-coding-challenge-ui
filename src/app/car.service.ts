import { Injectable } from '@angular/core';
import {Car} from './car';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  // TODO: Fix this REST URI
  private carsBaseUrl = 'http://localhost:8080/V1/api';

  constructor(private http: HttpClient) { }

  findAllCars(): Observable<Car[]> {
    return this.http.get<Car[]>(`${this.carsBaseUrl}`).pipe(
      catchError(this.handleError([]))
    );
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
